import { Component, OnInit } from '@angular/core';
import {AboutService} from '../about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  infos: any;
  comments = [];

  commentaire = {
    date : null,
    message : ''
  };

  constructor(private aboutService: AboutService) {
    this.infos    = this.aboutService.getInfo();
    this.comments = this.aboutService.getAllComments();
  }

  onAddCommentaire(commentaire) {
    this.aboutService.addComment(commentaire)
    this.commentaire.message = '';
    this.comments = this.aboutService.getAllComments();
  }

  ngOnInit() {
  }

}
