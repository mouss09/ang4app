import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class GalleryService {

  constructor(private http: Http) { }

  search(motCle: string, size: number, currentPage: number) {
    return this.http.get('https://pixabay.com/api/?key=7946841-ed2c0fc4df609c39135d7e7d7&q=' + motCle + '&per_page=' + size + '&page=' + currentPage)
      .map(resp => resp.json());
  }

}
