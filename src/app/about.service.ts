import { Injectable } from '@angular/core';

@Injectable()
export class AboutService {


  infos = {
    nom : 'Mouss',
    email : 'mouss@live.de',
    tel : '0987564312'
  };

  comments = [
    { date: new Date(), message : 'lol' },
    { date: new Date(), message : 'loup' },
    { date: new Date(), message : 'lion' }
  ];


  constructor() { }

  addComment(commentaire) {
    commentaire.date = new Date();
    this.comments.push(commentaire);
  }

  getAllComments() {
    return this.comments;
  }

  getInfo() {
    return this.infos;
  }
}
